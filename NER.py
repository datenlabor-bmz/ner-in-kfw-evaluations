from langchain.document_loaders import PyPDFLoader
from langchain.document_loaders import PyPDFDirectoryLoader
import os
from flair.data import Sentence
from flair.models import SequenceTagger
import pandas as pd
import torch
from timeit import default_timer as timer

# start timer to track performance
start = timer()

#device_id = torch.cuda.current_device() if torch.cuda.is_available() else -1
# -1 == CPU ; 0 == GPU
#print(device_id)

def entities_tagging(file, tagger):
    print("load documents")
    loader = PyPDFLoader(file)
    pages = loader.load()
    
    print("clean text and predict NER tags")
    texts = [Sentence(page.page_content) for page in pages]

    print("tagging")
    tagger.predict(texts)
    
    print("iterate over entities and store ner in list")
    entities_list = []

    for text in texts:
        for entity in text.get_spans('ner'):
            entities_list.append({
                'text': entity.text,
                'type': entity.tag,
                'score': entity.score
            })


    return entities_list, pages

def content_tagging(file: str, tagger):
    entities_list, pages = entities_tagging(file, tagger)

    print("transform list to dataframe") 
    entities_df = pd.DataFrame(entities_list)

    print("subset df for locations")
    locations = entities_df.loc[entities_df.loc[:,"type"] == "LOC",:]
    
    print("delete duplicates")
    locations = locations.drop_duplicates(subset="text", keep="first")

    print("add document index")
    locations["filename"] = [pages[0].metadata["source"].split("/")[-1] for _ in range(len(locations))]
    
    print("write to csv")
    locations.to_csv("test.csv", mode="a")
    print(f"Done: {file}")


def folder_content_tagging():
    # check directory
    os.getcwd()
    
    # check whether file already exists; if TRUE delete file
    if os.path.exists("test.csv"): 
        os.remove("test.csv")
    # get all files as list    
    files = os.listdir("data/")

    # load tagger for german NER
    german_flair = SequenceTagger.load("flair/ner-german")

    # for each file in directory data apply tagging function
    for file in files:
        content_tagging("data/" + file, tagger = german_flair)
    
    # done
    print("Done")

end = timer()
print(end - start) # time in seconds -> ~9 minutes for 10 documents; ~900 minutes for 1000 documents = 15 hrs

if __name__ == '__main__':
    folder_content_tagging()





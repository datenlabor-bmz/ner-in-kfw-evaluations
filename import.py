files = os.listdir("data/")

for file in files:
    loader = PyPDFLoader(file)
    pages = loader.load()    
    for page in pages:
        document = document + page.page_content
        text = Sentence(document)
        tagger.predict(text)
        for entity in text.get_spans('ner'):
             entities_list.append({
                 'text': entity.text,
                 'type': entity.tag,
                 'score': entity.score
                 })
